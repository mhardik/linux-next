// SPDX-License-Identifier: GPL-2.0+ OR MIT
/*
 * Apple MacBook Pro (13-inch, M1, 2022)
 *
 * target-type: J493
 *
 * Copyright The Asahi Linux Contributors
 */

/dts-v1/;

#include "t8112.dtsi"
#include "t8112-jxxx.dtsi"

/ {
	compatible = "apple,j493", "apple,t8112", "apple,arm-platform";
	model = "Apple MacBook Pro (13-inch, M2, 2022)";

	aliases {
		bluetooth0 = &bluetooth0;
		wifi0 = &wifi0;
	};
};

/*
 * Force the bus number assignments so that we can declare some of the
 * on-board devices and properties that are populated by the bootloader
 * (such as MAC addresses).
 */
&port00 {
	bus-range = <1 1>;
	wifi0: wifi@0,0 {
		compatible = "pci14e4,4425";
		reg = <0x10000 0x0 0x0 0x0 0x0>;
		/* To be filled by the loader */
		local-mac-address = [00 00 00 00 00 00];
		apple,antenna-sku = "XX";
		brcm,board-type = "apple,kyushu";
	};

	bluetooth0: bluetooth@0,1 {
		compatible = "pci14e4,5f69";
		reg = <0x10100 0x0 0x0 0x0 0x0>;
		/* To be filled by the loader */
		local-bd-address = [00 00 00 00 00 00];
		brcm,board-type = "apple,kyushu";
	};
};

&i2c4 {
	status = "okay";
};
